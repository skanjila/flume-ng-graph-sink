package org.apache.flume.sink.graph.client;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.flume.Event;
import org.apache.flume.conf.Configurable;
import org.neo4j.graphdb.Node;

import org.apache.flume.Event;
import org.apache.flume.conf.Configurable;
import org.neo4j.graphdb.Relationship;

import java.util.List;
import java.util.Map;

/**
 * Interface for an Neo4j client which is responsible for sending bulks
 * of events to Neo4j.
 */
public interface Neo4jClient extends Configurable {

    /**
     * Close connection to neo4j in client
     */
    void close();

    /**
     * Insert a new index event.
     *
     * @param event
     *    Flume Event
     * @param indexName
     *    an index name to use when creating the index
     * @throws Exception
     */
    public void addIndexEvent(Event event,String indexName) throws Exception;

    /**
     * Add a new insert node event to the bulk.  Assumes a pre-existing index.
     *
     * @param event
     *    Flume Event
     * @param node
     *    a node to be inserted into neo4j
     * @throws Exception
     */
    public void processAddNodeEvent(Event event,Node node) throws Exception;

    /**
     * Add a new relationship event to the bulk.  Assumes a pre-existing index.
     *
     * @param event
     *    Flume Event
     * @param relationship
     *    the relationship to add to the graph
     * @throws Exception
     */
    public void processAddRelationshipEvent(Event event,Relationship relationship) throws Exception;


    /**
     * Add a delete relationship event to the bulk.  Assumes a pre-existing index.
     *
     * @param event
     *    Flume Event
     * @param relationship
     *    the relationship to add to the graph
     * @throws Exception
     */
    public void processRemoveRelationshipEvent(Event event,Relationship relationship) throws Exception;


    /**
     * Add a new delete node event to the bulk.  Assumes a pre-existing index.
     *
     * @param event
     *    Flume Event
     * @param node
     *    a node to be inserted into neo4j
     * @throws Exception
     */
    public void processDeleteNodeEvent(Event event,Node node) throws Exception;


    /**
     * Sends bulk to the neo4j cluster
     *
     * @throws Exception
     */
    void execute() throws Exception;
}
