package org.apache.flume.sink.graph.client;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.neo4j.driver.v1.*;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 * This is the neo4j client which uses the bolt
 * protocol to perform the following operations
 * in bulk on a remote or local neo4j instance:
 * 1) add a node
 * 2) add a relationship
 * 3) remove a node
 * 4) remove a relationship
 */
public class Neo4jBoltClient implements Neo4jClient {

    private final org.neo4j.driver.v1.Driver driver;


    public Neo4jBoltClient(String url, String username, String password) {
        boolean hasPassword = password != null && !password.isEmpty();
        AuthToken token = hasPassword ? AuthTokens.basic(username, password) : AuthTokens.none();
        driver = GraphDatabase.driver(url, token, Config.build().withEncryptionLevel(Config.EncryptionLevel.NONE).toConfig());
    }


    @Override
    public void close() {
    }

    @Override
    public void addIndexEvent(Event event, String indexName) throws Exception {
        if (driver!=null) {
            final Transaction transaction = driver.session().beginTransaction();
        }

    }

    @Override
    public void processAddNodeEvent(Event event, Node node) throws Exception {
    }

    @Override
    public void processAddRelationshipEvent(Event event, Relationship relationship) throws Exception {
    }

    @Override
    public void processRemoveRelationshipEvent(Event event, Relationship relationship) throws Exception {
    }

    @Override
    public void processDeleteNodeEvent(Event event, Node node) throws Exception {
    }

    @Override
    public void execute() throws Exception {
    }

    @Override
    public void configure(Context context) {

    }
}
