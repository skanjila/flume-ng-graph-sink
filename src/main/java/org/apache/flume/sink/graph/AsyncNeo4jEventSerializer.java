/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.flume.sink.graph;

import java.util.List;

import org.apache.flume.Event;
import org.apache.flume.conf.Configurable;
import org.apache.flume.conf.ConfigurableComponent;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.Result;

/**
 * Interface for an event serializer which serializes the headers and body
 * of an event to write them to a particular neo4j index. This is configurable, so any config
 * params required should be taken through this.
 *  An implementation of this interface is expected by the {@linkplain AsyncNeo4jSink} to serialize
 * the events.
 */
public interface AsyncNeo4jEventSerializer extends Configurable, ConfigurableComponent {

  /**
   * Initialize the event serializer.
   * @param table - The Index the serializer should use when creating
   * {@link org.hbase.async.PutRequest} or
   * {@link org.hbase.async.AtomicIncrementRequest}.
   * @param index - The index to be used.
   */
  public void initialize(Index index);

  /**
   * @param event Event to be written to Neo4j
   */
  public void setEvent(Event event);

  /**
   * Get the actions that should be written out to neo4j as a result of this
   * event. This list is actually a
   * @return List of {@link String} which
   * represent cipher queries to be executed against neo4j.
   *
   *
   */
  public List<Result> getActions();



  /**
   * Clean up any state. This will be called when the sink is being stopped.
   */
  public void cleanUp();
}
